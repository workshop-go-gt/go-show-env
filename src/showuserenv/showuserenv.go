package main

import (
	"fmt"
	"os"
)

func main() {
	var user string
	var homeDir string

	user = os.Getenv("USER")
	homeDir = os.Getenv("HOME")

	fmt.Printf("Halo %s", user)
	fmt.Printf("\nHome anda di %s", homeDir)
	fmt.Printf("\n")
}
